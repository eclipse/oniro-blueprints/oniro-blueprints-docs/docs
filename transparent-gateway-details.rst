.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

Transparent Gateway Blueprint Details
#####################################

The following section details about various specifics of the Transparent Gateway 
Blueprint. The details include the basic networking subnet, configuration, 
Wi-Fi, and OpenThread connectivity details.

.. _rcp-flash:

Build and Flash the Radio Co-Processor Firmware
***********************************************
Build a OpenThread radio co-processor firmware for the Arduino Nano 33 BLE, this 
will act as a Thread radio for the Raspberry Pi board. 

To build the RCP image initialize a new build environment using the Zephyr 
build flavour:

.. code-block:: console

   $ cd $CHECKOUT_DIR
   $ TEMPLATECONF=../oniro/flavours/zephyr . ./oe-core/oe-init-build-env build-oniro-zephyr

Run the following command from the newly created build environment:

.. code-block:: console

   $ MACHINE=arduino-nano-33-ble DISTRO=oniro-zephyr bitbake zephyr-openthread-rcp

After building the image, we can now flash the Arduino board. Prepare the board 
by connecting the board's USB port to your computer and putting the board into 
flashing mode by double-pressing the reset button.

Next, use the following command to flash the board:

.. code-block:: console

   $ MACHINE=arduino-nano-33-ble DISTRO=oniro-zephyr bitbake zephyr-openthread-rcp -c flash_usb

Once you flashed the Arduino board, it can be connected to one of the USB ports 
on the Raspberry Pi board.

.. note::

   The *nRF52840 DK* and *nRF52840 Dongle* are alternative boards that can also 
   be used as RCP. In order to build and flash one of these boards, please 
   replace the `MACHINE` variable to ``nrf52840dk-nrf52840`` or 
   ``nrf52840-mdk-usb-dongle`` accordingly.  

.. _QR-gen:

Generate Onboarding QR Code
***************************

During the OpenThread onboarding process of the mesh node onto the OpenThread 
network, a QR code is used. The QR code holds the joiner credential, and the 
unique EUI64 address of the device to join.

The joiner credential can be chosen freely, but ensure that it is identical in 
the firmware of the node to join, as well as in the QR code.

In this blueprint the joiner credential is: J01NU5

The EUI64, basically the MAC address of the device, is hardware dependent and 
needs to be fetched from the device after flashing the generated firmware image. 
After accessing the serial console of the Arduino Nano 33 BLE, issue the 
*ot eui64* command to receive the address as follows:

.. code-block:: bash

   $ screen /dev/ttyACM0 115200
   uart:~$ ot eui64

The full string needed to be encoded in the QR code looks as follows:

`v=1&&eui=00124b001ca77adf&&cc=J01NU5`

The value for the EUI parameter needs to be replaced with the one received from
the device. The QR code needs to be generated in text format (not as an URL). 
A simple online QR generator can be found here: https://qrd.by/#text

Network Subnets and Configuration
*********************************

On the basis of the board you use, and its hardware configuration, the available 
network interfaces and their names may vary. In order to avoid confusion, the 
following interfaces are assumed to be available:

- **Ethernet interface eth0:** assumed to be uplink with DHCP enabled.
- **Wi-Fi interface wlan0:** Wi-Fi access point interface serving the Wi-Fi subnet.
- **OpenThread interface wpan0:** OpenThread border router interface serving the 
  mesh network.

In terms of IP subnets, use the private 172.16.47.0/24 range on the Wi-Fi 
subnet. The Access point(AP) has 172.16.47.1/24 assigned to it. Clients are 
served DHCP leases in the range of 172.16.47.100 - 172.16.47.150. The default 
DNS servers are 9.9.9.9 (primary) and 8.8.8.8 (secondary) respectively. 
For IPv6, you can rely on address auto-configuration for the time being.

On the OpenThread mesh network subnet, no IPv4 is available, and again you can 
rely on address auto-configuration for the time being.

Forwarding for IPv4 and IPv6 is enabled on all interfaces with `sysctl`.

Wi-Fi Access Point Configuration
********************************

Using the default Wi-Fi access point configuration, create an AP on channel 6 in 
the 2.4 GHz band with WPA2 pre-shared key configuration as:

.. code-block:: bash

   `SSID: "Oniro Project WiFi"`
   `Passphrase: "12345678"`

For more details, refer to the `NetworkManager configuration file <https://gitlab.eclipse.org/eclipse/oniro-blueprints/core/meta-oniro-blueprints-core/-/blob/master/recipes-connectivity/networkmanager/networkmanager-softap-config/SoftAP.nmconnection>`_ to get more clarity.

OpenThread Border Router Configuration
**************************************

In our default OpenThread border router configuration, we create an OpenThread 
mesh network on channel 26 in the 2.4 GHz band with panid 0x1357:

.. code-block:: bash

   `Networkname "OniroThread"`
   `OpenThread masterkey: 00112233445566778899aabbccddeeff`

For more details, refer to the `OpenThread configuration script <https://gitlab.eclipse.org/eclipse/oniro-blueprints/core/meta-oniro-blueprints-core/-/blob/master/recipes-connectivity/openthread/ot-br-posix/otbr-configuration>`_ for getting more clarity.

.. _mesh-troubleshooting:

Troubleshooting
***************

In case you run into trouble while onboarding the mesh node to the OpenThread
network this section has instructions how to factory reset the different 
components.

To factory reset the mesh node you need to connect to its serial console and
issue the following command, the device will reboot after the command:

.. code-block:: bash

   $ screen /dev/ttyACM0 115200
   uart:~$ ot factoryreset

To reset the Android Thread Commissioner App you need to clear data as well as
cache in the storage menu of the App info settings. Depending on the phone and
Android version there might be different ways to reach the App info settings. A
long press on the icon on the launcher could work or an application menu inside
the main Android settings dialog.
